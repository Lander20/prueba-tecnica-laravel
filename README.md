# Prueba Técnica Laravel

## *Objetivo*

Este test evaluará algunas de tus capacidades que nos interesan como desarrollador.

## *Requisitos*
1. PHP 7.1 o superior.
2. Composer 1.7 o superior
3. MySQL 8.0

## *Instrucciones de uso*
1. Hacer un Fork del proyecto.
2. `cp .env-example .env && php artisan key:generate`
3. Configurar el .env con una base de datos creada en MySQL (El nombre de la DB da lo mismo).
4. Ejecutar migraciones con `php artisan migrate && php artisan db:seed`
5. Levantar el proyecto con `php artisan serve`
6. Revisar correcto funcionamiento en `http://127.0.0.1:8000`

## *Instrucciones de desarrollo*
Desarrollar un servicio API RESTful que pueda mostrar e ingresar proyectos y pagos de proyectos.
Sólo se pueden realizar estas operaciones si el usuario que hace la solicitud está registrado y logueado en el sistema, por lo que tambien deben existir endpoints para el registro, login y logout.

Luego del registro el sistema debe enviar un E-Mail de confirmación, el cual debe incluirse en una cola de trabajos de Artisan.
El rol del usuario es irrelevante para realizar las operaciones.

Para la seguridad del servicio se puede usar Passport o similares.

El servicio debe ser compatible con Cross Origin Resource Sharing (CORS).

Entregar desarrollo por medio de un pull-request y notificar envío por email

## *Bonus (suma puntos)*
1. Entregar un front en VueJs para pruebas de CORS.
2. Unit testing con PHPUnit.

## *En que nos fijaremos*

* Correcto uso de Eloquent
* Correcto uso de GIT
* Patrones de diseño
* Orden del código


## Email
* Los emails funcionan con mailtrap, por lo que se requiere que se genere una cuenta.


##Instalación

## *Instrucciones de instalación*
1. Instalar dependencias de COMPOSER `composer install`
2. Instalar dependencias de NPM `npm i`
3. Ejecutar npm `npm run dev` o `npm run production`
4. Configurar environment
5. Ejecutar migraciones con `php artisan migrate && php artisan db:seed`
6. Ejecutar `php artisan passport:key`
7. Ejecutar `php artisan passport:client --personal`
8. Levantar el proyecto con `php artisan serve`
9. Ejecutar la cola `php artisan queue:work` o `php artisan queue:listen`

