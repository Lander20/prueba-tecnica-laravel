<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function (){
    return redirect('/login');
});

Auth::routes();
Route::get('logout', function (){
    Auth::logout();
    return redirect('/login');
});

Route::middleware(['auth'])->group(function () {
    Route::resource('proyecto', 'ProyectoController');
    Route::resource('pago', 'ProyectoPagoController');

    Route::get('proyecto/{proyecto}/pago', 'ProyectoPagoController@index')->name('pago.index');
    Route::get('proyecto/{proyecto}/pago/create', 'ProyectoPagoController@create')->name('pago.create');
    Route::post('proyecto/{proyecto}/pago/store', 'ProyectoPagoController@store')->name('pago.store');
});

