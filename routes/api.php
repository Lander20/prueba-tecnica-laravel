<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function(){
    Route::resource('proyecto', 'Api\ProyectoController');
    Route::resource('pago', 'Api\ProyectoPagoController');

    Route::get('proyecto/{proyecto}/pago', 'Api\ProyectoPagoController@index');
    Route::post('proyecto/{proyecto}/pago/store', 'Api\ProyectoPagoController@store');
});

Route::post('login', 'UserApiController@login');
Route::post('register', 'UserApiController@register');
Route::post('logout', 'UserApiController@logout');

