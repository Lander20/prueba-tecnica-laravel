@extends('layouts.app')

@section('content')
    <pago-list :proyecto="{{$proyecto}}" :pagos="{{ $proyecto->pagos }}" >
        <p class="text-center">Cargando lista de pagos ...</p>
    </pago-list>
@endsection
