@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                {{--<div class="card-header">Dashboard</div>--}}
                <div class="card-body">
                    <a href="{{route('proyecto.create')}}" class="btn btn-success float-right">
                        Crear nuevo
                    </a>
                    <div class="col-md-12 table-responsive">
                        @include('proyecto.index',[$proyectos])
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
