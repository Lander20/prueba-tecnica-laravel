@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <div class="col-md-12">
                            {!! Form::open([ 'route' => 'proyecto.store']) !!}
                            @include('proyecto.form')
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success',"style"=>"display:block; margin: 0 auto"]) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
