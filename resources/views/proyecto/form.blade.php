<div class="form-group">
    {{ Form::label('Nombre', null, ['class' => 'control-label']) }}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {{ Form::label('Monto', null, ['class' => 'control-label']) }}
    {!! Form::number('monto', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {{ Form::label('Cantidad de cuotas', null, ['class' => 'control-label']) }}
    {!! Form::number('cuotas', null, ['class' => 'form-control']) !!}
</div>