<h5>Proyectos</h5>

<table class="table table-striped">
    <thead>
    <tr>
        <th>Nombre</th>
        <th>Monto</th>
        <th>Coutas</th>
        <th>Acción</th>
    </tr>
    </thead>
    <tbody>
    @foreach($proyectos as $proyecto)
        <tr>
            <td> {{ $proyecto->nombre }} </td>
            <td> {{ $proyecto->monto }} </td>
            <td> {{ $proyecto->cuotas }} </td>
            <td>
                <a class="btn btn-info" href="{{route('proyecto.edit',$proyecto->id)}}"> Editar / Ver </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>