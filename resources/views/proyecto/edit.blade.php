@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <a class="btn btn-info float-right" href="{{route('pago.index',$proyecto->id)}}"> Ver pagos </a>

                        <div class="col-md-12" style="margin-top: 20px">
                            {!! Form::model($proyecto, ['method' => 'PATCH', 'route' => ['proyecto.update',$proyecto->id]]) !!}
                            @include('proyecto.form')
                            {!! Form::submit('Guardar', ['class' => 'btn btn-success float-right']) !!}
                            {!! Form::close() !!}

                            {!! Form::model($proyecto, ['method' => 'DELETE', 'route' => ['proyecto.destroy',$proyecto->id]]) !!}
                            {!! Form::submit('Eliminar', ['class' => 'btn btn-danger float-left']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
