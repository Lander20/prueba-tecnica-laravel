require('./bootstrap');

import PagoList from './components/PagoList.vue'
import PagoEdit from './components/PagoEdit.vue'
import PagoCreate from './components/PagoCreate.vue'

new Vue({
    el: '#app',
    components: {
        PagoList,
        PagoEdit,
        PagoCreate
    }
})
