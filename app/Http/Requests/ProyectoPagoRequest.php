<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoPagoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'cuota'    => 'required|integer',
            'fecha'    => 'required',
            'monto'    => 'required|integer',
        ];
    }

    public function messages(){
        return [
            'cuota.required'   => 'La cuota es requerido',
            'fecha.required'   => 'La fecha es requerido',
            'monto.required'    => 'El monto es requerido',

            'cuota.integer'    => 'La cuota debe ser un número',
            'monto.integer'     => 'El monto debe ser un número',
        ];
    }
}
