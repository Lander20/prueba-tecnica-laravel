<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(){
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        return [
            'nombre'    => 'required',
            'monto'     => 'required|integer',
            'cuotas'    => 'required|integer',
        ];
    }

    public function messages(){
        return [
            'nombre.required'   => 'El nombre es requerido',
            'monto.required'    => 'El monto es requerido',
            'cuotas.required'   => 'La cantidad de cuotas es requerido',

            'monto.integer'     => 'El monto debe ser un número',
            'cuotas.integer'    => 'La cantidad de cuotas debe ser un número',
        ];
    }
}
