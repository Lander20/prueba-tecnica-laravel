<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginApiRequest;
use App\Jobs\SendEmail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserApiController extends Controller{

    public function register(LoginApiRequest $request){
        $data = [
            'email'     => request()->get('email'),
            'password' => Hash::make(request()->get('password')),
            'name'     => request()->get('name'),
        ];
        $user = User::create($data);
        SendEmail::dispatchNow($user->name , $user->email);
        return response()->json([
            'message' => 'Usuario creado'
        ], 201);
    }

    public function login(LoginApiRequest $request){
        $this->validateAttempt();

        $infoToken = $request->user()->createToken('PAT');
        $infoToken->token->expires_at = Carbon::now()->addDays(1);

        $infoToken->token->save();
        $data =  [
            'token_type' => 'Bearer',
            'access_token' => $infoToken->accessToken,
            'expires_at' => Carbon::parse($infoToken->token->expires_at)->toDateTimeString()
        ];
        return response()->json($data);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Usuario deslogueado'
        ]);
    }

    private function validateAttempt(){
        if( !Auth::attempt(request(['email', 'password'])) ){
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    }

}
