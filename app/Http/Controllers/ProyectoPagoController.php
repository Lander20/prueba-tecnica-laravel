<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProyectoPagoRequest;
use App\Proyecto;
use App\ProyectoPago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProyectoPagoController extends Controller{

    public function index($proyecto){
        $proyecto = Proyecto::findOrFail($proyecto);
        return view('pago.index', compact('proyecto'));
    }

    public function create($proyecto){
        return view('pago.create', compact('proyecto'));
    }

    public function store(Proyecto $proyecto, Request $request){
        $proyecto->pagos()->create(request()->all());

        Session::flash('flash_message', 'Pago creado con éxito !');
        return redirect()->route('pago.index',$proyecto);
    }

    public function edit(ProyectoPago $pago){
        return view('pago.edit', compact('pago'));
    }

    public function update(ProyectoPagoRequest $request, ProyectoPago $pago){
        $pago->update(request()->all());
        Session::flash('flash_message', 'Pago actualizado con éxito !');
        return redirect()->route('pago.index', $pago->proyecto->id);
    }

    public function destroy(ProyectoPago $pago){
        $proyecto = $pago->proyecto->id;
        $pago->delete();
        Session::flash('flash_message', 'Pago eliminado con éxito !');
        return redirect()->route('pago.index', $proyecto);
    }
}
