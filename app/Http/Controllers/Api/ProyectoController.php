<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProyectoRequest;
use App\Proyecto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProyectoController extends Controller{

    public function index(){
        $proyectos = Proyecto::all();
        return response()->json(['data' => $proyectos]);
    }

    public function update(ProyectoRequest $request, Proyecto $proyecto){
        $proyecto->update(request()->all());
        return response()->json(['data' => $proyecto, 'message' => 'Proyecto actualizado con éxito']);
    }

    public function store(ProyectoRequest $request){
        $proyecto = Proyecto::create(request()->all());
        return response()->json(['data' => $proyecto, 'message' => 'Proyecto creado con éxito']);
    }

    public function destroy(Proyecto $proyecto){
        $proyecto->delete();
        return response()->json(['data' => [], 'message' => 'Proyecto eliminado con éxito']);
    }
}
