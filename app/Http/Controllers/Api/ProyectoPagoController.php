<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ProyectoPagoRequest;
use App\Proyecto;
use App\ProyectoPago;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProyectoPagoController extends Controller{

    public function index(Proyecto $proyecto){
        return response()->json(['data' => $proyecto->pagos]);
    }

    public function store(Proyecto $proyecto, Request $request){
        $pago = $proyecto->pagos()->create(request()->all());
        return response()->json(['data' => $pago, 'message' => 'Pago ingresado con éxito']);
    }

    public function update(ProyectoPagoRequest $request, ProyectoPago $pago){
        $pago->update(request()->all());
        return response()->json(['data' => $pago, 'message' => 'Pago actualizado con éxito']);
    }

    public function destroy(ProyectoPago $pago){
        $pago->delete();
        return response()->json(['data' => [], 'message' => 'Pago eliminado con éxito']);
    }

}
