<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProyectoRequest;
use App\Proyecto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ProyectoController extends Controller{

    public function index(){
        $proyectos = Proyecto::all();
        return view( 'home', compact('proyectos'));
    }

    public function edit(Proyecto $proyecto){
        return view('proyecto.edit', compact('proyecto'));
    }

    public function update(ProyectoRequest $request, Proyecto $proyecto){
        $proyecto->update(request()->all());
        Session::flash('flash_message', 'Proyecto actualizado con éxito !');
        return back();
    }

    public function create(){
        return view('proyecto.create');
    }

    public function store(ProyectoRequest $request){
        Proyecto::create(request()->all());
        Session::flash('flash_message', 'Proyecto creado con éxito !');
        return redirect()->route('proyecto.index');
    }

    public function destroy(Proyecto $proyecto){
        $proyecto->delete();
        Session::flash('flash_message', 'Proyecto eliminado con éxito !');
        return redirect()->route('proyecto.index');
    }
}
