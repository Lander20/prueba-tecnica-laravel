<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyecto extends Model{
    use SoftDeletes;

    protected $fillable = [
        'nombre', 'monto', 'cuotas'
    ];

    public function pagos(){
        return $this->hasMany(ProyectoPago::class);
    }
}
