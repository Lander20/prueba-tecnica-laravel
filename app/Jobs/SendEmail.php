<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $fullname;
    public $email;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fullname, $email){
        $this->fullname = $fullname;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $email = $this->email;
        $data = ['name' => $this->fullname];

        Mail::send('email.register', $data, function ($message) use ($email) {
            $message->from('no-reply@prueba-laravel.cl', 'Prueba Laravel');
            $message->to($email)->subject('Registro de usuario');
        });
    }
}
