<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProyectoPago extends Model{

    protected $fillable = [
        'cuota', 'fecha', 'monto'
    ];

    protected $hidden = [
        'created_at', 'deleted_at', 'updated_at'
    ];

    public function proyecto(){
        return $this->belongsTo(Proyecto::class);
    }

}
